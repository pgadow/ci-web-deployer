# CERN CI Web Deployer
Docker image to be used with GitLab CI to deploy web sites or generic files or binaries generated from GitLab repositories to any of the CERN Web services platform including EOS.

## Version

1.8
  - Use SMB3 protocol for DFS by default, but allow users to configure it through the `SMB_PROTOCOL` variable.

1.7
  - Support deployment to EOS personal/project/workspace folder

1.6
  - Moved docker image to the GitLab registry

1.5
  - Support for [CERN DFS websites](https://espace.cern.ch/webservices-help/websitemanagement/ConfiguringCentrallyHostedSites-IIS7/Pages/default.aspx)

## Contents
* *deploy-dfs*: Publish in a DFS folder the contents provided. Makes use of environment variables, *Kerberos* for authentication and *smbclient* to interact with DFS.
  * `DFS_WEBSITE_USERNAME`: NICE username of the user responsible of the deployment
  * `DFS_WEBSITE_PASSWORD`: User's password
  * `CI_WEBSITE_DIR` (optional): Local folder to where files/folders to be deployed are located. **Default:** `public/`
  * `DFS_WEBSITE_NAME`: Name of the DFS website (Example: "test-gitlab-pages" whose URL would be https://test-gitlab-pages.web.cern.ch)
  * `DFS_WEBSITE_DIR` (optional): A subfolder of the DFS web site where to deploy pages (e.g. `mydocs` to publish to https://test-gitlab-pages.web.cern.ch/mydocs). **Default** publish to web site root folder.
  * `SMB_PROTOCOL` (optional): Set the SAMBA protocol to be used for the transfer. By default, it is set to `smb3`

* *deploy-eos*: Publish in a EOS folder the contents provided. Makes use of environment variables, *Kerberos* for authentication and *lxplus.cern.ch* as bridge to access to EOS.
  * `EOS_ACCOUNT_USERNAME`: NICE username of the user responsible of the EOS deployment. Must have RW access to the EOS folder
  * `EOS_ACCOUNT_PASSWORD`: User's password
  * `CI_OUTPUT_DIR` (optional): Local folder to be rsynced with EOS folder. **Default:** `public/`
  * `EOS_PATH`: EOS path where to deploy the contents generated in `CI_OUTPUT_DIR`
  * `EOS_MGM_URL` (optional): The MGM URL of the EOS instance. **Default:** `root://eosuser.cern.ch`

## Download
```sh
docker pull gitlab-registry.cern.ch/ci-tools/ci-web-deployer
```
