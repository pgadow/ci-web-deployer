#!/bin/bash

# April 2016 Borja Aparicio
# Receives:
# Environment variables
#   DFS_WEBSITE_USERNAME
#   DFS_WEBSITE_PASSWORD
#   CI_WEBSITE_DIR => default: public/
#   DFS_WEBSITE_NAME
#   SMB_PROTOCOL => default: smb3
#
# Produces:
#  Uploads to --dfs-website-path in DFS the files found in --source-dir

# SMBCLIENT present to connect to Windoes DFS filesystem
smbclient="/usr/bin/smbclient"
if [ ! -x $smbclient ]
then
        echo "ERROR: $smbclient not found"
        exit 1
fi

# Set smbclient protocol from input
smbclient="${smbclient} -m ${SMB_PROTOCOL:-smb3}"

# Authenticate user via Kerberos
kinit="/usr/bin/kinit"
if [ ! -x $kinit ]
then
        echo "ERROR: $kinit not found"
        exit 1
fi

kdestroy="/usr/bin/kdestroy"
if [ ! -x $kdestroy ]
then
        echo "ERROR: $kdestroy not found"
        exit 1
fi

# Validate input
: "${DFS_WEBSITE_NAME:?DFS_WEBSITE_NAME not provided}"
: "${DFS_WEBSITE_USERNAME:?DFS_WEBSITE_USERNAME not provided}"
: "${DFS_WEBSITE_PASSWORD:?DFS_WEBSITE_PASSWORD not provided}"

# Protected or configuration files in DFS. Don't override or delete this with the deploy job
if [ "X$DFS_PROTECTED_FILES" == "X" ]
then
	DFS_PROTECTED_FILES="web.config:robots.txt:Global.asax"
fi

# Directory where the web site has been generated in the CI environment
# If not provided by the user
if [ "X$CI_WEBSITE_DIR" == "X" ]
then
	CI_WEBSITE_DIR="public/"
fi

# Check the source directory exists
if [ ! -d $CI_WEBSITE_DIR ]
then
	echo "ERROR: Source directory '$CI_WEBSITE_DIR' doesn't exist"
	exit 1
fi

# Get credentials
echo "$DFS_WEBSITE_PASSWORD" | $kinit "$DFS_WEBSITE_USERNAME@CERN.CH" 2>&1 >/dev/null
if [ $? -ne 0 ]
then
	echo "Failed to get Krb5 credentials for '$DFS_WEBSITE_USERNAME'"
        exit 1
fi

# From the Website name, we build the path in DFS
# If DFS_WEBSITE_NAME is "test-gitlab-pages", DFS_WEBSITE_PATH is "\Websites\t\test-gitlab-pages"
# If a value is provided for DFS_WEBSITE_DIR (e.g. "mydir") then  DFS_WEBSITE_PATH becomes "\Websites\t\test-gitlab-pages\mydir"
# Get the first letter of the website
website_prefix=${DFS_WEBSITE_NAME:0:1}
DFS_WEBSITE_PATH="\Websites\\$website_prefix\\$DFS_WEBSITE_NAME\\$DFS_WEBSITE_DIR"

# Check the target directory in DFS exists and the user has the needed permissions
$smbclient -k //cerndfs.cern.ch/dfs -c "cd $DFS_WEBSITE_PATH"
if [ $? -ne 0 ]
then
	echo "ERROR: Failed to access '//cerndfs11/dfs/$DFS_WEBSITE_PATH'"
        exit 1
fi

# TENTATIVE TO KEEP A LIST OF PROTECTED FILES (CONFIG FILES)
# IN ORDER FOR THE DEPLOYER TO DO A FULL SYNC OF THE FILES PRODUCED BY THE USER
# A BACKUP OF THE PROTECTED FILES HAS TO BE DONE, THEN REMOVED EVERYTHING IN DFS
# THEN, RESTORE THE PROTECTED FILES AND FINALLY COPY THE FILES PROVIDED BY THE USER
# WITH SMBCLIENT, THERE IS NOT A SIMPLE WAY TO DO A rm -rf ./website/*

# FOR THE MOMENT JUST OVERIDE WHAT THE USER PROVIDES

# Copy locally the protected files
#if [ -d temp ]
#then
#	rm -rf temp
#fi
#mkdir temp

#protected_files=$(echo $DFS_PROTECTED_FILES | tr ":" "\n")
#for file in $protected_files
#do
#	$smbclient -k //cerndfs11/dfs -c "lcd temp; cd $DFS_WEBSITE_PATH; prompt OFF; mget $file;"
#done

# Remove everything in DFS
#$smbclient -k //cerndfs11/dfs -c "cd $DFS_WEBSITE_PATH; recurse ON; prompt OFF; mask ""; rmdir *"

#if [ $? -ne 0 ]
#then
#        echo ERROR: Failed to delete //cerndfs11/dfs/$DFS_WEBSITE_PATH
#        exit 1
#fi

# Restore the protected files
#$smbclient -k //cerndfs11/dfs -c "lcd temp; cd $DFS_WEBSITE_PATH; recurse; prompt OFF; mask *; mput *"
#if [ $? -ne 0 ]
#then
#	echo ERROR: Failed to update protected files in //cerndfs11/dfs/$DFS_WEBSITE_PATH
#        exit 1
#fi

# Copy the contents to the folder specified
$smbclient -k //cerndfs11/dfs -c "lcd $CI_WEBSITE_DIR; cd $DFS_WEBSITE_PATH; recurse; prompt OFF; mask *; mput *"
if [ $? -ne 0 ]
then
	echo "ERROR: Failed to update '//cerndfs11/dfs/$DFS_WEBSITE_PATH'"
        exit 1
fi

# Destroy credentials
$kdestroy
if [ $? -ne 0 ]
then
	echo "Krb5 credentials for '$DFS_WEBSITE_USERNAME' have not been cleared up"
fi

#rm -rf temp

echo "Updated DFS web site in '$DFS_WEBSITE_PATH'"
exit 0
